<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('tgl_lahir');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('provinsi');
            $table->string('kota');
            $table->string('kec');
            $table->string('desa');
            $table->text('alamat');
            $table->string('postal');
            $table->string('ktp')->unique();
            $table->string('position');
            $table->string('bank_name');
            $table->string('bank_account');
            $table->text('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
