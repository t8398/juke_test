<?php

namespace Database\Seeders;
use App\Models\Bank;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = array(
            array('name' => 'BANK MANDIRI'),
            array('name' => 'BANK BRI'),
            array('name' => 'BANK BNI'),
            array('name' => 'BANK BCA'),
            array('name' => 'BANK LAINNYA'),
           
         );
         foreach ($positions as $position) {
            Bank::create([
              'name' => $position['name']
            ]);
         }
    }
}
