<?php

namespace Database\Seeders;
use App\Models\Position;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = array(
            array('name' => 'MANAGER'),
            array('name' => 'SUPERVISOR'),
            array('name' => 'STAFF'),
            array('name' => 'ADMIN'),
            array('name' => 'LAINNYA'),
           
         );
         foreach ($positions as $position) {
            Position::create([
              'name' => $position['name']
            ]);
         }
    }
}
