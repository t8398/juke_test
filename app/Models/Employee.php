<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';
    protected $primaryKey ='id';
    protected $fillable = [
        'first_name',
        'last_name',
        'tgl_lahir',
        'phone',
        'email',
        'provinsi',
        'kota',
        'kec',
        'desa',
        'alamat',
        'postal',
        'ktp',
        'position',
        'bank_name',
        'bank_account',
        'image',

  ];
    public $timestamps = true;
    public function posisi() {
        return $this->hasOne(Position::class, 'id', 'position');
    }
    public function bank() {
        return $this->hasOne(Bank::class, 'id', 'bank_name');
    }
}
