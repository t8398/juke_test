<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Bank;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $pegawai = Employee::get();;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'tgl_lahir'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'provinsi'=>'required',
            'kota'=>'required',
            'kec'=>'required',
            'desa'=>'required',
            'alamat'=>'required',
            'postal'=>'required',
            'ktp'=>'required',
            'position'=>'required',
            'bank_name'=>'required',
            'bank_account'=>'required',
            'image'=>'required|image',
        ]);
        try{
            $imageName = Str::random().'.'.$request->image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('employee/image', $request->image,$imageName);
            Employee::create($request->post()+['image'=>$imageName]);
            return response()->json([
                'message'=>'Employee Created Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while creating a Employee List !!'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return response()->json([
            'employee'=>$employee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'tgl_lahir'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'provinsi'=>'required',
            'kota'=>'required',
            'kec'=>'required',
            'desa'=>'required',
            'alamat'=>'required',
            'postal'=>'required',
            'ktp'=>'required',
            'position'=>'required',
            'bank_name'=>'required',
            'bank_account'=>'required',
            'image'=>'nullable',
        ]);
        try{
            $employee->fill($request->post())->update();
            if($request->hasFile('image')){
                // remove old image
                if($employee->image){
                    $exists = Storage::disk('public')->exists("employee/image/{$employee->image}");
                    if($exists){
                        Storage::disk('public')->delete("employee/image/{$employee->image}");
                    }
                }
                $imageName = Str::random().'.'.$request->image->getClientOriginalExtension();
                Storage::disk('public')->putFileAs('employee/image', $request->image,$imageName);
                $employee->image = $imageName;
                $employee->save();
            }
            return response()->json([
                'message'=>'Employee List Updated Successfully!!'
            ]);
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while updating a Employee List !!'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        try {
            if($employee->image){
                $exists = Storage::disk('public')->exists("employee/image/{$employee->image}");
                if($exists){
                    Storage::disk('public')->delete("employee/image/{$employee->image}");
                }
            }
            $employee->delete();
            return response()->json([
                'message'=>'Employee List Deleted Successfully!!'
            ]);
            
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json([
                'message'=>'Something goes wrong while deleting a Employee List !!'
            ]);
        }
    }
}
